This describes a docker container with [Elm](http://elm-lang.org/) installed via
`npm`. It can be used to develop and manage Elm projects.

# Usage

By default, this container will run `elm reactor` on whichever project is
mounted to the `/work` directory. This can be changed by overwriting the
[CMD](https://docs.docker.com/engine/reference/builder/#/cmd) which is run
automatically by the entrypoint script: `/init.sh`.

`/init.sh` will also run additional initialization scripts ending in `*.sh` that
it finds in the `/docker-entrypoint-init.d` directory. These scripts are sourced
in alphabetical order.