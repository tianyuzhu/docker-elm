FROM node:slim

RUN mkdir /init.d
VOLUME /init.d
COPY init.sh /init.sh

WORKDIR /work
VOLUME /work
ENTRYPOINT [ "/init.sh" ]
CMD [ "/usr/local/bin/elm-reactor", "--address=0.0.0.0", "--port=8000" ]
EXPOSE 8000

RUN npm install -g elm@0.18.0
