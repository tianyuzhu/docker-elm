#!/usr/bin/env bash

for file in /docker-entrypoint-init.d/*; do
  case "$file" in
    *.sh)   echo "$0: running $file"; . "$file" ;;
    *)      echo "$0: ignoring $file" ;;
  esac
  echo
done
exec "$@"
